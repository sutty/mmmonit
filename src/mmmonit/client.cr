require "http/client"

# Monit Client
# ============
#
# When a Monit instance registers with M/Monit, it sends its own
# credentials so M/Monit can call back with actions.
#
# To perform an action, first we need to log in using basic
# authentication and store the CSRF token returned with the cookie.  Any
# action requires to send confirmation through the `securitytoken`
# field.
module Mmmonit
  class Client
    getter username : String
    getter password : String
    getter hostname : String

    @http_client : HTTP::Client
    @cookie : HTTP::Cookie

    # Set username, password and hostname
    def initialize(@username, @password, @hostname)
      @http_client = http_client
      @cookie = authenticate!
    end

    # Performs an action for a service
    def perform(service : String, action : String) : Bool
      headers  = HTTP::Headers{"Cookie" => @cookie.to_cookie_header}
      form     = { securitytoken: @cookie.value, action: action }
      response = http_client.post("/#{service}", headers: headers, form: form)

      response.status_code == 200
    end

    # Instantiates an HTTP client to call back the Monit instance.
    def http_client : HTTP::Client
      HTTP::Client.new(@hostname, 2812).tap do |c|
        c.basic_auth(@username, @password)
      end
    end

    # Calls the Monit instance and gets a security token for the next
    # action.
    def authenticate! : HTTP::Cookie
      http_client.get("/").cookies["securitytoken"]
    end
  end
end

require "kemal"
require "uuid"
require "crometheus"

# Only store in disk what's happening so we can figure out how to get
# actual data later.
#
# @todo Add Basic Authentication
post "/collector" do |env|
  uuid = UUID.random

  File.open(File.join(".", "storage", "xml", "#{uuid}.xml"), "w") do |xml|
    IO.copy(env.request.body || IO::Memory.new, xml)
  end

  env.response.status_code = 200
end

add_handler Crometheus::Middleware::HttpCollector.new
add_handler Crometheus.default_registry.get_handler

Kemal.run

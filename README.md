## Plans

### Collect information about Monit instances in containers

At Sutty, every container run its own Monit instance, performing
self-monitoring, scheduling, etc.

### Serve as a collectively administered panel

People should be able to see infrastructure status and be welcome to
help monitor and restarting services if they're down.

Services aren't self describing, so adding an explanation to them
helps learning what's happening.

Mmmonit provides the information and facilitates action.  A Hotwired
Jekyll site provides the rest. (Though this means Mmmonit should render
partial information as HTML frames, consumable by Jekyll.)

Jekyll provides the context and explanation for services and how they
interact ("literate sysadmin").

### Allow Prometheus to scrape this information

So we can have other kinds of querying, alerts, etc.

### Act as a proxy for sending actions to Monits

Ability to restart a service, but only if it's down already.


## Installation

TODO: Write installation instructions here

## Usage

TODO: Write usage instructions here

## Development

TODO: Write development instructions here

## Contributing

1. Fork it (<https://0xacab.org/sutty/mmmonit/fork>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [f](https://0xacab.org/fauno)
